BIN = "${HOME}/.local/bin"
VENV = "${HOME}/.local/share/venvs/gplt"

.PHONY: install uninstall venv

install:
	mkdir -p "${BIN}"
	cp src/main.py "${BIN}/gplt"
	chmod +x "${BIN}/gplt"
	cp src/plot.py "${BIN}/"
	cp src/multi_bar.py "${BIN}/"
	cp src/error.py "${BIN}/"
	cp src/log.py "${BIN}/"
	./scripts/set-version.sh "${BIN}"

uninstall:
	rm "${BIN}/gplt"
	rm "${BIN}/plot.py"
	rm "${BIN}/multi_bar.py"
	rm "${BIN}/error.py"
	rm "${BIN}/log.py"

venv:
	./scripts/create-venv.sh "${VENV}"
