#!/usr/bin/env bash

[[ -z $1 ]] && {
    echo "please give the path to the venv directory"
    exit 1
}

virtualenv "$1" --prompt gplt
source "$1/bin/activate"
pip install -r requirements.txt
