#!/usr/bin/env bash

[[ -z $1 ]] && {
    echo "please give the path to the installation directory"
    exit 1
}

sed --in-place "s/{{VERSION}}/$(git describe)/g" "$1/gplt"
