from enum import Enum


class GPLTError(Enum):
    SUCCESS = 0
    INVALID_ARGUMENTS = 1
    MODULE_SHOULD_NOT_RUN = 2
    COULD_NOT_DECODE_JSON = 3
    COULD_NOT_CONVERT = 4
    NO_SUCH_FILE = 5
