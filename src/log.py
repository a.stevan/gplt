from error import GPLTError
from rich import print


def panic(err: GPLTError, msg: str):
    match err:
        case GPLTError.SUCCESS:
            x = "SUCCESS"
        case GPLTError.INVALID_ARGUMENTS:
            x = "INVALID_ARGUMENTS"
        case GPLTError.MODULE_SHOULD_NOT_RUN:
            x = "MODULE_SHOULD_NOT_RUN"
        case GPLTError.COULD_NOT_DECODE_JSON:
            x = "COULD_NOT_DECODE_JSON"
        case GPLTError.COULD_NOT_CONVERT:
            x = "COULD_NOT_CONVERT"
        case GPLTError.NO_SUCH_FILE:
            x = "NO_SUCH_FILE"
        case _:
            print(f"[bold red]PANIC[/bold red]: unexpected error: {err} is not known")
            exit(1)

    print(f"[bold red]PANIC[/bold red]: [red]{x}[/red]: {msg}")
    exit(err.value)


def success(msg: str = None):
    if msg is not None:
        print(msg)
    exit(GPLTError.SUCCESS.value)


def info(msg: str):
    print(f"[bold cyan]INFO[/bold cyan]: {msg}")


def warning(msg: str):
    print(f"[bold yellow]WARNING[/bold yellow]: {msg}")


def debug(msg: str):
    print(f"[bold green]DEBUG[/bold green]: {msg}")
