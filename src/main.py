#!/usr/bin/env python
import argparse
from typing import Tuple
import matplotlib.pyplot as plt

import plot
import multi_bar
from error import GPLTError
from log import panic, success, info

import os
import json

VERSION = "{{VERSION}}"


def load_json(json_value: str, filename: str = None):
    if filename is not None:
        if not os.path.exists(filename):
            panic(
                GPLTError.NO_SUCH_FILE,
                f"{filename}: no such file",
            )
        info(f"loading JSON data from {filename}")
        with open(filename, "r") as file:
            try:
                data = json.load(file)
            except json.decoder.JSONDecodeError as e:
                panic(
                    GPLTError.COULD_NOT_DECODE_JSON,
                    f"Could not decode JSON data from {filename}: {e}"
                )
    else:
        try:
            data = json.loads(json_value)
        except json.decoder.JSONDecodeError as e:
            panic(
                GPLTError.COULD_NOT_DECODE_JSON,
                f"Could not decode JSON data from STDIN: {e}"
            )

    return data


def _parse_float_or_none(v: str, msg: str) -> float | None:
    if v == "null":
        return None
    else:
        try:
            return float(v)
        except ValueError as e:
            panic(
                GPLTError.COULD_NOT_CONVERT,
                f"{msg}: {e}",
            )


def _parse_plot_bounds(
    bounds: Tuple[str, str] | None,
    msg: str,
) -> Tuple[float | None, float | None]:
    if bounds is None:
        return (None, None)

    low, high = bounds
    return (
        _parse_float_or_none(low, msg=msg),
        _parse_float_or_none(high, msg=msg),
    )


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        "gplt",
        description="A general plotter script in Python",
        formatter_class=argparse.RawTextHelpFormatter,
    )
    parser.add_argument("--version", "-v", action="store_true")

    subparsers = parser.add_subparsers(title="subcommands", dest="subcommand")

    plot_parser = subparsers.add_parser(
        "plot",
        help="Simple plot supporting multiple graphs and custom styling.",
        formatter_class=argparse.RawTextHelpFormatter,
    )
    plot_parser.add_argument("graphs", type=str, nargs="?", help=f"the list of graphs to plot\n\n{plot.HELP}")
    plot_parser.add_argument(
        "--legend-loc",
        "-l",
        type=str,
        choices=[
            "best",
            "upper right",
            "upper left",
            "lower left",
            "lower right",
            "right",
            "center left",
            "center right",
            "lower center",
            "upper center",
            "center"
        ],
        nargs="*",
        default=[],
        help="the locations of the legends"
    )
    plot_parser.add_argument("--no-legend", action="store_true")
    plot_parser.add_argument("--x-label", "-x", type=str, help="the x label of the plot")
    plot_parser.add_argument("--y-label", "-y", type=str, help="the y label of the plot")
    plot_parser.add_argument("--x-scale", "-X", type=str, choices=["linear", "log"], default="linear", help="the x scale of the plot")
    plot_parser.add_argument("--x-scale-base", type=int, default=None, help="the base of the x scale")
    plot_parser.add_argument("--y-scale", "-Y", type=str, choices=["linear", "log"], default="linear", help="the y scale of the plot")
    plot_parser.add_argument("--y-scale-base", type=int, default=None, help="the base of the y scale")
    plot_parser.add_argument("--x-lim", nargs=2, help="limit the x axis of the plot, use `null` for open bounds")
    plot_parser.add_argument("--y-lim", nargs=2, help="limit the y axis of the plot, use `null` for open bounds")
    plot_parser.add_argument("--x-ticks-rotation", type=float, help="the rotation angle, in degrees, of the ticks of the X axis")
    plot_parser.add_argument("--x-ticks-rotation-horizontal-alignment", type=str, default="left", help="the rotation horizontal alignment")
    plot_parser.add_argument("--x-ticks-rotation-mode", type=str, default="anchor", help="the rotation mode")
    plot_parser.add_argument("--x-ticks", type=float, nargs='*', help="the ticks for the X axis")
    plot_parser.add_argument("--x-tick-labels", type=str, nargs='*', help="the tick labels for the X axis, should be the same length as `--x-ticks`")
    plot_parser.add_argument("--y-ticks", type=float, nargs='*', help="the ticks for the Y axis")
    plot_parser.add_argument("--y-tick-labels", type=str, nargs='*', help="the tick labels for the Y axis, should be the same length as `--y-ticks`")
    plot_parser.add_argument("--font", type=str, default="{}", help="the font parameters")
    plot_parser.add_argument("--square-grid", action="store_true")

    multi_bar_parser = subparsers.add_parser(
        "multi-bar",
        help="Multi-bar plot.",
        formatter_class=argparse.RawTextHelpFormatter,
    )
    multi_bar_parser.add_argument("data", type=str, nargs="?", help=f"the actual data to show in a multibar plot\n\n{multi_bar.HELP}")
    multi_bar_parser.add_argument("--label", "-l", type=str, help="the measurement label of the multibar plot")
    multi_bar_parser.add_argument("--style", type=str, default="{}", help="specification for the style of the bar plots, in JSON")
    multi_bar_parser.add_argument("--log-scale", action="store_true")
    multi_bar_parser.add_argument("--grid", action="store_true")
    multi_bar_parser.add_argument("--minor-grid", action="store_true")
    multi_bar_parser.add_argument("--grid-behind", action="store_true")

    common_options = [
        {
            "flags": ["--json-data-file", "-f"],
            "kwargs": {"type": str, "help": "an alternate path to the JSON data"}
        },
        {
            "flags": ["--title", "-t"],
            "kwargs": {"type": str, "help": "the title of the figure"}
        },
        {
            "flags": ["--fullscreen"],
            "kwargs": {"action": "store_true"}
        },
        {
            "flags": ["--save", "-s"],
            "kwargs": {"type": str, "help": "a path to save the figure to"}
        },
        {
            "flags": ["--fig-size"],
            "kwargs": {"type": float, "nargs": 2, "default": [16, 9], "help": "the size, in inches, of the saved image"}
        },
        {
            "flags": ["--dpi"],
            "kwargs": {"type": int, "default": 500, "help": "the Dots Per Inch (DPI) of the saved image"}
        },
        {
            "flags": ["--use-tex"],
            "kwargs": {"action": "store_true"}
        },
    ]
    for option in common_options:
        plot_parser.add_argument(*option["flags"], **option["kwargs"])
        multi_bar_parser.add_argument(*option["flags"], **option["kwargs"])

    args = parser.parse_args()
    if args.version:
        success(VERSION)
    if args.subcommand is None:
        success("nothing to do: see `--help`")

    if args.subcommand == "plot":
        graphs = load_json(args.graphs, filename=args.json_data_file)

        plot.check(graphs)

        fig = plot.plot(
            graphs,
            args.title,
            args.x_label,
            args.y_label,
            plot_layout="constrained" if args.fullscreen else None,
            legend_loc=args.legend_loc,
            show_legend=not args.no_legend,
            x_scale=args.x_scale,
            x_scale_base=args.x_scale_base,
            y_scale=args.y_scale,
            y_scale_base=args.y_scale_base,
            x_lim=_parse_plot_bounds(args.x_lim, msg="X limits"),
            y_lim=_parse_plot_bounds(args.y_lim, msg="Y limits"),
            x_ticks=args.x_ticks,
            x_tick_labels=args.x_tick_labels,
            x_ticks_rotation=args.x_ticks_rotation,
            x_ticks_rotation_ha=args.x_ticks_rotation_horizontal_alignment,
            x_ticks_rotation_mode=args.x_ticks_rotation_mode,
            y_ticks=args.y_ticks,
            y_tick_labels=args.y_tick_labels,
            font=json.loads(args.font),
            use_tex=args.use_tex,
            square_grid=args.square_grid,
        )
    elif args.subcommand == "multi-bar":
        groups, measurements = multi_bar.extract(load_json(args.data, filename=args.json_data_file))

        plot_layout = "constrained" if args.fullscreen else None

        fig = multi_bar.multi_bar(
            groups,
            measurements,
            args.title,
            args.label,
            plot_layout=plot_layout,
            use_tex=args.use_tex,
            log_scale=args.log_scale,
            grid=args.grid,
            minor_grid=args.minor_grid,
            grid_behind=args.grid_behind,
            style=json.loads(args.style),
        )

    if fig is None:
        success()

    if args.save is not None:
        filename = os.path.abspath(os.path.expanduser(args.save))
        fig.set_size_inches(args.fig_size, forward=False)
        plt.savefig(filename, dpi=args.dpi)

        info(f"plot saved as {filename}")
    else:
        plt.show()
