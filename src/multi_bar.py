from typing import Dict, List

import matplotlib.pyplot as plt
import matplotlib
import numpy as np
import shutil

from error import GPLTError
from log import panic, warning


HELP = """# Example
```json
{
    "age": {
        "alice": 31,
        "bob": 28,
        "charlie": 44
    },
    "height": {
        "alice": 1.50,
        "bob": 1.82,
        "charlie": 1.65
    },
    "weight": {
        "alice": 65.3,
        "bob": 98.1,
        "charlie": 68.7
    }
}
```
and the style could be something like
```json
{
  "alice": {
    "color": [ 0.6823529411764706, 0.7764705882352941, 0.8117647058823529 ]
  },
  "bob": {
    "color": [ 1, 0.4117647058823529, 0.3803921568627451 ]
  },
  "charlie": {
    "color": [ 0.4666666666666667, 0.8666666666666667, 0.4666666666666667 ]
  }
}
```"""


# convert raw data into groups and measurements
#
# # Example
# input is the same as in [`HELP`]
#
# output:
# ```
# groups = ["age", "height", "weight"]
# measurements = {
#      "alice": [1.50, 31, 65.3],
#      "bob": [1.82, 28, 98.1],
#      "charlie": [1.65, 44, 68.7]
# }
# ```
def extract(data: Dict[str, Dict[str, float]]) -> (List[str], Dict[str, List[float]]):
    groups = list(data.keys())

    measurements = {}
    for x in data[groups[0]].keys():
        measurements[x] = [data[g][x] for g in groups]

    return (groups, measurements)


# plot multi bars
#
# input can be the output of [`extract`], see [`HELP`]
def multi_bar(
    groups: List[str],
    measurements: Dict[str, List[float]],
    title: str,
    y_label: str,
    labels_locations: List[float] = None,
    width: float = 0.25,
    nb_legend_cols: int = 3,
    legend_loc: str = "upper left",
    plot_layout: str = "constrained",
    use_tex: bool = False,
    log_scale: bool = False,
    grid: bool = False,
    minor_grid: bool = False,
    grid_behind: bool = False,
    style: dict = {},
) -> matplotlib.figure.Figure:
    if labels_locations is None:
        labels_locations = np.arange(len(groups))

    if use_tex:
        if shutil.which("latex") is None:
            warning("LaTeX is not installed, using default rendering")
        else:
            plt.rcParams.update({
                "text.usetex": True,
                'text.latex.preamble': r'\usepackage{amsfonts}'
            })

    fig, ax = plt.subplots(layout=plot_layout)

    zorder = 3 if grid_behind else None
    for i, (attribute, measurement) in enumerate(measurements.items()):
        rects = ax.bar(labels_locations + width * i, measurement, width, zorder=zorder, label=attribute, **style.get(attribute, {}))
        ax.bar_label(rects, padding=3)

    ax.set_ylabel(y_label)
    ax.set_title(title)
    if log_scale:
        ax.set_yscale('log')
    if (grid or minor_grid) and grid_behind:
        ax.yaxis.grid(zorder=0)
    if grid:
        ax.yaxis.grid(True, which="major")
    if minor_grid:
        ax.yaxis.grid(True, which="minor")
    ax.set_xticks(labels_locations + width, groups)
    ax.legend(loc=legend_loc, ncols=nb_legend_cols)

    return fig


if __name__ == "__main__":
    panic(GPLTError.MODULE_SHOULD_NOT_RUN, msg="running this module won't do anything")
