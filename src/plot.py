import matplotlib.pyplot as plt
import matplotlib
import numpy as np
import shutil
from itertools import zip_longest

from error import GPLTError
from log import panic, warning, debug, info

from typing import Any, Dict, List, Optional, Tuple, TypedDict


class Point(TypedDict):
    x: float
    y: float
    e: Optional[float]


class MarkerStyle(TypedDict):
    shape: Optional[str]
    size: Optional[int]


class LineStyle(TypedDict):
    marker: Optional[MarkerStyle]
    type: Optional[str]
    width: Optional[int]
    alpha: Optional[float]


class ErrorStyle(TypedDict):
    alpha: Optional[float]


class Style(TypedDict):
    color: Optional[str]
    line: Optional[LineStyle]
    error: Optional[ErrorStyle]


class Graph(TypedDict):
    name: Optional[str]
    legend: Optional[str]
    points: List[Point]
    style: Optional[Style]


class Font(TypedDict):
    family: str
    serif: str
    weight: str
    size: int


# # Example
# ```python
# class Foo(typing.TypedDict):
#     a: str
#     b: int
#     c: Optional[List[float]]
# ```
# calling [`__get_class_fields`] on `Foo` will give
# ```python
# {
#     "a": False,
#     "b": False,
#     "c": True,
# }
# ```
def __get_class_fields(c) -> Dict[str, bool]:
    d = {}
    for k, v in vars(c)["__annotations__"].items():
        d[k] = str(v).startswith("typing.Optional")
    return d


HELP = """## Example
```nuon
[
    {
        name: "Alice", # optional, unset or set to null won't show the grap name
        legend: "main", # optional, not required for simple plots
        points: [
            [ x, y, e ];
            [ 1, 1143, 120 ],
            [ 2, 1310, 248 ],
            [ 4, 1609, 258 ],
            [ 8, 1953, 343 ],
            [ 16, 2145, 270 ],
            [ 32, 3427, 301 ]
        ],
        style: {},  # optional, see section below
    },
    {
        name: "Bob", # optional, unset or set to null won't show the grap name
        legend: "main", # optional, not required for simple plots
        points: [
            [ x, y, e ];
            [ 1, 2388, 374 ],
            [ 2, 2738, 355 ],
            [ 4, 3191, 470 ],
            [ 8, 3932, 671 ],
            [ 16, 4571, 334 ],
            [ 32, 4929, 1094 ]
        ]
        style: {},  # optional, see section below
    },
]
```

## Custom style
any record inside the data can have an optional "style" specification.

below is the full shape of that specification, where all of the keys are completely optional,
default values have been chosen:
```nuon
{
    color: null,  # see https://matplotlib.org/stable/users/explain/colors/colors.html
    line: {
        marker: {
            shape: "o",  # see https://matplotlib.org/stable/api/markers_api.html
            size: 5,
        },
        type: null,  # see https://matplotlib.org/stable/gallery/lines_bars_and_markers/linestyles.html
        width: null,  # just an integer
        alpha: 1.0,  # a real number between 0 and 1
    },
    error: {
        alpha: 0.3,  # a real number between 0 and 1
    },
}
```

## Parametrizing the font
> see https://matplotlib.org/stable/users/explain/text/usetex.html

below is an example of the Helvetica font using _serifs_, best rendered with
`--use-tex`
```nuon
{
    size: 15,
    family: serif,
    sans-serif: Helvetica,
}
```"""


def _parse_graph_points(pts: List[Point], name: str) -> Tuple[List[float], List[float], List[float]]:
    xs = list(filter(lambda x: x is not None, [p.get("x") for p in pts]))
    ys = list(filter(lambda x: x is not None, [p.get("y") for p in pts]))
    es = list(filter(lambda x: x is not None, [p.get("e") for p in pts]))

    if len(xs) != len(ys):
        panic(
            GPLTError.INVALID_ARGUMENTS,
            f"found {len(xs)} x values and {len(ys)} y values for points of graph {name}"
        )

    if not len(es) in [0, len(xs)]:
        panic(
            GPLTError.INVALID_ARGUMENTS,
            f"please provide error values for all points or for none, found {len(es)} errors for {len(xs)} points for graph {name}"
        )

    return xs, ys, es


def _parse_graph_style(g: Graph) -> (Dict[str, Any], float):
    style = {
        "marker": 'o',
        "markersize": 5,
        "linestyle": None,
        "color": None,
        "linewidth": None,
        "alpha": 1.0,
    }

    custom_style = g.get("style", {})
    style["color"] = custom_style.get("color", None)
    style["marker"] = custom_style.get("line", {}).get("marker", {}).get("shape", style["marker"])
    style["markersize"] = custom_style.get("line", {}).get("marker", {}).get("size", style["markersize"])
    style["linestyle"] = custom_style.get("line", {}).get("type", style["linestyle"])
    style["linewidth"] = custom_style.get("line", {}).get("width", style["linewidth"])
    style["alpha"] = custom_style.get("line", {}).get("alpha", style["alpha"])

    error_alpha = custom_style.get("error", {}).get("alpha", 0.3)

    return style, error_alpha


def _plot_single(g: Graph, i: int, ax: matplotlib.axes.Axes) -> matplotlib.lines.Line2D:
    name = g.get('name', i)

    xs, ys, es = _parse_graph_points(g["points"], name)
    if len(xs) == 0:
        warning(f"no point to plot for graph {name}")

    line_style, error_alpha = _parse_graph_style(g)

    line = ax.plot(xs, ys, label=g.get("name", None), **line_style)[0]

    if len(es) != 0:
        debug(f"adding errors to graph {name}")
        ys, es = np.array(ys), np.array(es)
        down = ys - es
        up = ys + es
        if line_style["color"] is None:
            ax.fill_between(xs, down, up, alpha=error_alpha)
        else:
            ax.fill_between(xs, down, up, alpha=error_alpha, color=line_style["color"])

    return line


def _check_extra_class_fields(
    obj: Dict[str, Any],
    name: str,
    fields: Dict[str, bool],
    sub_path: List[str] = None
):
    for f in obj.keys():
        if f not in fields.keys():
            full_path = '.'.join(sub_path + [f]) if sub_path is not None else f
            warning(f"field '{full_path}' of {name} is unexpected")


def _check_class_fields(
    obj: Dict[str, Any],
    name: str,
    fields: Dict[str, bool],
    sub_path: List[str] = None
):
    for f, is_optional in fields.items():
        if not is_optional and f not in obj.keys():
            full_path = '.'.join(sub_path + [f]) if sub_path is not None else f
            panic(
                GPLTError.INVALID_ARGUMENTS,
                f"missing required field '{full_path}' in {name}",
            )

    _check_extra_class_fields(obj, name, fields, sub_path=sub_path)


def check(graphs: Any):
    info("checking graphs")

    if not isinstance(graphs, list):
        panic(
            GPLTError.INVALID_ARGUMENTS,
            f"input is not a list, found {type(graphs).__name__}",
        )

    graph_fields = __get_class_fields(Graph)
    point_fields = __get_class_fields(Point)

    for i, g in enumerate(graphs):
        debug(f"checking graph {i}")
        if not isinstance(g, dict):
            panic(
                GPLTError.INVALID_ARGUMENTS,
                f"expected dict at index {i}, found {type(g).__name__}",
            )

        name = g.get('name', i)

        _check_class_fields(g, f"graph {name}", graph_fields)

        for j, p in enumerate(g["points"]):
            _check_class_fields(
                p, f"graph {name}", point_fields, sub_path=["points", str(j)]
            )

        if "style" in g:
            info(f"checking the `$.style` of graph {name}")
            _check_extra_class_fields(
                g["style"], name, __get_class_fields(Style), sub_path=["style"]
            )

            if "line" in g["style"]:
                _check_extra_class_fields(
                    g["style"]["line"],
                    name,
                    __get_class_fields(LineStyle),
                    sub_path=["style", "line"]
                )
                if "marker" in g["style"]["line"]:
                    _check_extra_class_fields(
                        g["style"]["line"]["marker"],
                        name,
                        __get_class_fields(MarkerStyle),
                        sub_path=["style", "line", "marker"]
                    )

            if "error" in g["style"]:
                _check_extra_class_fields(
                    g["style"]["error"],
                    name,
                    __get_class_fields(ErrorStyle),
                    sub_path=["style", "error"]
                )


# see [`HELP`]
def plot(
    graphs: List[Graph],
    title: str,
    x_label: str,
    y_label: str,
    plot_layout: str = "constrained",
    legend_loc: List[str] = [],
    show_legend: str = True,
    x_scale: str = "linear",
    x_scale_base: int = None,
    y_scale: str = "linear",
    y_scale_base: int = None,
    x_lim: Tuple[float | None, float | None] = (None, None),
    y_lim: Tuple[float | None, float | None] = (None, None),
    x_ticks: List[float] = None,
    x_tick_labels: List[str] = None,
    x_ticks_rotation: float = None,
    x_ticks_rotation_ha: str = "left",
    x_ticks_rotation_mode: str = "anchor",
    y_ticks: List[float] = None,
    y_tick_labels: List[str] = None,
    font_size: float = None,
    font: Font = {},
    use_tex: bool = False,
    square_grid: bool = False,
) -> matplotlib.figure.Figure:
    if len(graphs) == 0:
        warning("no graph to plot")
        return

    plt.rc("font", **font)

    if use_tex:
        if shutil.which("latex") is None:
            warning("LaTeX is not installed, using default rendering")
        else:
            plt.rcParams.update({
                "text.usetex": True,
                'text.latex.preamble': r'\usepackage{amsfonts}'
            })

    fig, ax = plt.subplots(layout=plot_layout)

    legends = {}
    for i, g in enumerate(graphs):
        info(f"plotting graph {i}")

        line = _plot_single(g, i, ax)
        legend = g.get("legend", "main" if "name" in g else None)
        if legend is not None:
            debug(f"adding legend of graph {i}")
            if legend not in legends:
                legends[legend] = []
            legends[legend] += [line]

    # ======================== axes ========================

    ax.set_xlabel(x_label)
    ax.set_ylabel(y_label)

    if x_scale == "linear":
        if x_scale_base is not None:
            warning("cannot set scale base on X axis with linear scale")
        ax.set_xscale("linear")
    else:
        if x_scale_base is None:
            warning("X axis has log scale but the base is not given, defaulting to base 10")
            x_scale_base = 10
        ax.set_xscale("log", base=x_scale_base)
    if y_scale == "linear":
        if y_scale_base is not None:
            warning("cannot set scale base on Y axis with linear scale")
        ax.set_yscale("linear")
    else:
        if y_scale_base is None:
            warning("Y axis has log scale but the base is not given, defaulting to base 10")
            y_scale_base = 10
        ax.set_yscale("log", base=y_scale_base)

    ax.set_xlim(x_lim)
    ax.set_ylim(y_lim)

    # ======================== grid ========================

    ax.grid(True, which="major")
    if x_scale == "log":
        ax.xaxis.grid(True, which="minor")
    if y_scale == "log":
        ax.yaxis.grid(True, which="minor")
    if x_scale == "log" or y_scale == "log":
        ax.minorticks_on()

    if square_grid:
        ax.axis("equal")

    # ======================== ticks ========================

    if x_ticks is not None:
        labels = x_tick_labels if x_tick_labels is not None else x_ticks
        if len(x_ticks) != len(labels):
            panic(
                GPLTError.INVALID_ARGUMENTS,
                f"X ticks and their labels should have the same length, found {len(x_ticks)} and {len(labels)}"
            )
        ax.set_xticks(x_ticks, labels=labels)
    if y_ticks is not None:
        labels = y_tick_labels if y_tick_labels is not None else y_ticks
        if len(y_ticks) != len(labels):
            panic(
                GPLTError.INVALID_ARGUMENTS,
                f"Y ticks and their labels should have the same length, found {len(y_ticks)} and {len(labels)}"
            )
        ax.set_yticks(y_ticks, labels=labels)

    if x_ticks_rotation is not None:
        plt.xticks(
            rotation=x_ticks_rotation,
            ha=x_ticks_rotation_ha,
            rotation_mode=x_ticks_rotation_mode,
        )

    # ======================== final touch ========================

    ax.set_title(title)
    if show_legend:
        if legend_loc == []:
            ax.legend(loc=None)
        else:
            ax.legend(loc=legend_loc[0])

        extra_legends = []
        for loc, handles in zip_longest(legend_loc, legends.values()):
            if handles is not None:
                extra_legends.append(ax.legend(handles=handles, loc=loc))
        for legend in extra_legends[:-1]:
            plt.gca().add_artist(legend)

    return fig


if __name__ == "__main__":
    panic(GPLTError.MODULE_SHOULD_NOT_RUN, msg="running this module won't do anything")
